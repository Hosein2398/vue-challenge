import { createRouter, createWebHashHistory } from 'vue-router';
import state from '../state'
import Login from '../views/Login.vue';
import Dashboard from '../views/Dashboard.vue';
import SettingComopnent from '../components/Setting.vue';
import InvoicesComopnent from '../components/Invoices.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/dashboard',
    redirect: '/dashboard/setting',
    name: 'Dashboard',
    component: Dashboard,
    children: [
      {
        path: 'setting',
        component: SettingComopnent,
      },
      {
        path: 'invoices',
        component: InvoicesComopnent,
        beforeEnter: (to, from, next) => {
          if(state.userType !== 'admin'){
            next(false)
          } else {
            next()
          }
        }
      },
    ],
    beforeEnter: (to, from, next) => {
      if(!state.loggedInState){
        next({path:'/login', replace:true})
      } else{
        next()
      }
    }
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
