import { createServer } from 'miragejs'

const invoicesData = [
  {
    id:0,
    price:123,
    comment:'good'
  },
  {
    id:1,
    price:23,
    comment:'niceee for real'
  },
  {
    id:2,
    price:48,
    comment:'what is this'
  },
  {
    id:3,
    price:568,
    comment:'wwoww'
  }
];


export const makeServer = () => {
return createServer({
  routes() {
    this.namespace = 'api';

    this.post('/login', (schema, request) => {
      let attrs = JSON.parse(request.requestBody);
      const userClient = {
        username: 'client',
        pass: '123456'
      }
      const userAdmin = {
        username: 'admin',
        pass: '123456'
      }
      if(attrs.username === userClient.username && attrs.pass === userClient.pass){
        return {
          status: 'successful',
          type: 'client'
        }
      }
      if(attrs.username === userAdmin.username && attrs.pass === userAdmin.pass){
        return {
          status: 'successful',
          type: 'admin'
        }
      }

      return {
        status: 'failed'
      }

    });

    this.post('/add_invoice', (_, request) => {
      let attrs = JSON.parse(request.requestBody);
      const id = invoicesData.length
      console.log(id)
      invoicesData[id] = {}
      invoicesData[id].id = id
      invoicesData[id].price = attrs.price
      invoicesData[id].comment = attrs.comment
      console.log(invoicesData)
      return {
        status: 'successful'
      }
    })

    this.post('/update_invoice', (_, request) => {
      let attrs = JSON.parse(request.requestBody);

      invoicesData[attrs.id].comment = attrs.comment
      invoicesData[attrs.id].price = attrs.price
      return {
        status: 'successful'
      }
    })

    this.post('/update', (_, request) => {
      return {
        status: 'successful'
      }
    })

    this.get('/invoices', () => {
      return {
        data: invoicesData
      }
    });

    this.get('/test', () => {
      return {
        data: 'this is a test'
      }
    });
  },
});
}
