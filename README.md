# Vue challenge

## notes
- To log in `admin` and `client` usernames could be used, and password is 1-6 for both.
- There is no consistency in saving of user state since wasn't ask in spec. (no localstorage or anything, so refreshes will lead to login page for sure)
- Due to shortage of time form validations are comletely safe and sane.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
